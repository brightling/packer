#!/bin/bash -euxo pipefail

# Sudoers: Add Vagrant Passwordless Access
echo "Defaults:vagrant !requiretty" > /etc/sudoers.d/vagrant
echo "vagrant 	ALL=(ALL) 	NOPASSWD: ALL" >> /etc/sudoers.d/vagrant
chmod 440 /etc/sudoers.d/vagrant


# Disable unattended apt updates
echo 'APT::Periodic::Enable "0";' >> /etc/apt/apt.conf.d/10periodic


# Apt
export DEBIAN_FRONTEND=noninteractive
apt update

dpkg --list | awk '{ print $2 }' | grep 'linux-headers' | xargs apt -y purge
dpkg --list | awk '{ print $2 }' | grep 'linux-image-extra' | xargs apt -y purge
apt -y purge libx11-data libx11-6 xauth
apt -y purge popularity-contest installation-report friendly-recovery command-not-found command-not-found-data
apt -y purge cpp gcc g++ linux-firmware
apt -y dist-upgrade -o Dpkg::Options::="--force-confnew";
apt -y install linux-image-virtual
# Remove the current kernel if another has been installed
linux_image_count=$(dpkg --list | awk '{ print $2 }' | grep -e 'linux-image-[0-9].*-generic'| wc -l)
echo "Current kernel: $(uname -r)"
if [[ linux_image_count -gt 1 ]]; then
dpkg --list | awk '{ print $2 }' | grep "linux-image-.*$(uname -r)" | xargs apt -y purge
fi
apt -y install acl
apt -y autoremove --purge


# Vagrant SSH setup
mkdir -p ~/.ssh
wget -O ~/.ssh/authorized_keys https://raw.githubusercontent.com/hashicorp/vagrant/master/keys/vagrant.pub 
chown -R vagrant:vagrant ~/.ssh
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys


# Change to use eth network names
sed -i -e 's/GRUB_CMDLINE_LINUX=""/GRUB_CMDLINE_LINUX="net.ifnames=0 biosdevname=0"/g' /etc/default/grub
update-grub


#Fix Networking for vagrant
echo '# This file describes the network interfaces available on your system' > /etc/network/interfaces
echo '# and how to activate them. For more information, see interfaces(5).' >> /etc/network/interfaces
echo '' >> /etc/network/interfaces
echo 'source /etc/network/interfaces.d/*' >> /etc/network/interfaces
echo '' >> /etc/network/interfaces
echo '# The loopback network interface' >> /etc/network/interfaces
echo 'auto lo' >> /etc/network/interfaces
echo 'iface lo inet loopback' >> /etc/network/interfaces
echo '' >> /etc/network/interfaces
echo '# The primary network interface' >> /etc/network/interfaces
echo 'auto eth0' >> /etc/network/interfaces
echo 'iface eth0 inet dhcp' >> /etc/network/interfaces
echo '' >> /etc/network/interfaces

# Cleanup
apt clean
rm -rf /var/lib/apt/lists/*

# Reduce image size
dd if=/dev/zero of=/EMPTY bs=1M || :
rm -f /EMPTY
sync
